const user = require('../controllers/userController')
const crypt = require('../lib/crypt')
const jwt = require('jsonwebtoken')

module.exports = {
    login: async (req, res) => {
        let response = {}
        try {
            let userData = await user.readByUsername(req.body.username)
            if (userData) {
                if (crypt.compare(req.body.password, userData.password)) {
                    let userDataSent = {}
                    userDataSent.id = userData.id
                    userDataSent.username = userData.username
                    userDataSent.authority = userData.authority
                    userDataSent.biodata = userData.biodata
                    userDataSent.token = jwt.sign(userDataSent, 's3cr3t')

                    response = { ...userDataSent }
                    response.success = true
                }
            }
            else {
                res.status(401)
                response.message = 'invalid username or password'
                response.success = false

            }
        } catch (e) {
            res.status(401)
            response.message = e.message
            response.success = false
        }
        finally {
            res.json(response)
        }
    },
    verifyToken: async (req, res) => {
        let response = {}
        try {
            let { token } = req.body
            payload = jwt.verify(token,'s3cr3t')
            response = {tokenValid:true}
        } catch (e) {
            // res.status(401)
            // response.message = e.message
            if (e instanceof jwt.JsonWebTokenError) {
                // if the error thrown is because the JWT is unauthorized
                response = {tokenValid:false}
            }
        }
        finally {
            res.json(response)
        }
    }
}