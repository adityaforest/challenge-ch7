const { User, UserHistory } = require('../models')

module.exports = {
    create: async (req, res) => {
        try {
            await UserHistory.create({
                roomid: req.body.id,
                player1id: req.body.player1id,
                player1username: req.body.player1username,
                player2id: req.body.player2id,
                player2username: req.body.player2username,
                role: req.body.role,
                round1: req.body.round1,
                round2: req.body.round2,
                round3: req.body.round3
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
    },
    readAll: async (req, res) => {
        let result = []
        try {
            result = await UserHistory.findAll()
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    readByUserId: async (req, res) => {
        let userid = parseInt(req.params.userid)
        let result = []
        try {
            result = await UserHistory.findAll({
                where: {
                    userid: userid
                }
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    delete: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            let x = await UserHistory.destroy({
                where: { id: id }
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
    },
}