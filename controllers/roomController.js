let roomList = []

fightResult = (player1choice, player2choice) => {
    if (player1choice == "R") {
        if (player2choice == "R") {
            return "DRAW"
        }
        else if (player2choice == "P") {
            return "P2"
        }
        else if (player2choice == "S") {
            return "P1"
        }
    }
    else if (player1choice == "P") {
        if (player2choice == "R") {
            return "P1"
        }
        else if (player2choice == "P") {
            return "DRAW"
        }
        else if (player2choice == "S") {
            return "P2"
        }
    }
    else if (player1choice == "S") {
        if (player2choice == "R") {
            return "P2"
        }
        else if (player2choice == "P") {
            return "P1"
        }
        else if (player2choice == "S") {
            return "DRAW"
        }
    }
}

module.exports = {
    enter: (req, res) => {
        // check for unmatched room , if exist enter it
        for (let i in roomList) {
            console.log(roomList[i])
            if (!roomList[i].matched) {
                roomList[i].matched = true
                roomList[i].player2id = req.body.id
                roomList[i].player2username = req.body.username
                return res.json(roomList[i])
            }
        }

        //if not exist , create new room
        let arrayId = []
        for (let i in roomList) {
            arrayId.push(roomList[i].id)
        }
        let newId = 0
        if (arrayId.length > 0) {
            newId = (Math.max(...arrayId)) + 1
        }
        let newRoom = {
            id: newId,
            matched: false,
            player1id: req.body.id,
            player1username: req.body.username,
            gameDetail: [{}, {}, {}]
        }
        roomList.push(newRoom)

        res.json(newRoom)
    },
    match: (req, res) => {
        let roomid = parseInt(req.params.id)
        let username = req.params.username
        // let roomid = req.body.id
        // let username = req.body.username
        console.log(`opening new matching connection for roomid : ${roomid} username : ${username}`)

        res.set({
            "Cache-control": "no-cache",
            "Content-type": "text/event-stream",
            "Connection": "keep-alive"
        })
        res.flushHeaders()

        res.write("retry: 500\n\n")

        let id = 1
        let oldRoomData = {}
        let interval = setInterval(() => {
            let newRoomData = {}
            for (let i in roomList) {
                if (roomList[i].id == roomid) {
                    console.log(`room ${roomid} username : ${username} matching masih jalan bro`)
                    newRoomData = roomList[i]
                }
            }

            // if (oldRoomData == newRoomData) {
            //     return
            // }
            oldRoomData = newRoomData

            res.write(`id: ${id}\n`)
            res.write(`event: roomMatching\n`)
            res.write(`data: ${JSON.stringify(oldRoomData)}\n\n`)
            id++
            // console.log(oldRoomData)
            // console.log(`room ${roomid} matching masih jalan bro`)
            if (oldRoomData.matched) {
                clearInterval(interval)
                console.log(`closing matching connection for roomid : ${roomid} username : ${username}`)
                res.end()
            }
        }, 500)

        res.on("close", () => {
            clearInterval(interval)
            console.log(`closing matching connection for roomid : ${roomid} username : ${username}`)
            res.end()
        })
    },
    checkGameDetail: (req, res) => {
        let roomid = parseInt(req.params.roomid)
        for (let i in roomList) {
            if (roomList[i].id == roomid) {
                for (let x in roomList[i].gameDetail) {
                    roomList[i].gameDetail[x].winner = fightResult(roomList[i].gameDetail[x].p1choice, roomList[i].gameDetail[x].p2choice)
                }
                // console.log(roomList[i].gameDetail)
                return res.json(roomList[i].gameDetail)
            }
        }
    },
    playerChoose: (req, res) => {
        for (let i in roomList) {
            if (roomList[i].id == req.body.roomid) {
                if (req.body.role == "P1") {
                    roomList[i].gameDetail[req.body.round].p1choice = req.body.choice
                }
                else if (req.body.role == "P2") {
                    roomList[i].gameDetail[req.body.round].p2choice = req.body.choice
                }
                return res.json({success:true})
            }
        }
    },
    delete: (req, res) => {
        let id = parseInt(req.params.id)
        for (let i in roomList) {
            if (roomList[i].id == id) {
                roomList.splice(i, 1)
                return res.json({ deleteSuccess: true, message: "delete room success" })
            }
        }
        res.json({ deleteSuccess: false, message: "room id not exist" })
    }
}