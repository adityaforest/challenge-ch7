const { User, UserBiodata } = require('../models')
const crypt = require('../lib/crypt')

module.exports = {
    create: async (req, res) => {         
        try {
            let x = await User.create({
                username: req.body.username,
                password: crypt.encrypt(req.body.password),
                authority: "PlayerUser"
            })
            let y = await UserBiodata.create({
                id: x.id,
                email: req.body.email,
                firstName: req.body.firstName,
                lastName: req.body.lastName
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
    },
    readById: async (req, res) => {
        let id = parseInt(req.params.id)
        let result = {}
        try {
            result = await User.findOne({
                where: {
                    id: id
                },
                include: [{
                    model: UserBiodata,
                    as: `biodata`,
                    required: true
                }]
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    readByUsername: async (username) => {
        let result = {}
        try {
            result = await User.findOne({
                where: {
                    username: username
                },
                include: [{
                    model: UserBiodata,
                    as: `biodata`,
                    required: true
                }]
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
        finally {
            return result
        }
    },
    readAll: async (req, res) => {
        let result = []
        try {
            result = await User.findAll({
                include: [{
                    model: UserBiodata,
                    as: `biodata`,
                    required: false
                }]
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    update: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            let x = await User.update({
                username: req.body.username,
                password: req.body.password
            }, {
                where: {
                    id: id
                }
            })
            let y = await UserBiodata.update({
                email: req.body.email,
                firstName: req.body.firstName,
                lastName: req.body.lastName
            }, {
                where: {
                    id: id
                }
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
    },
    delete: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            let x = await User.destroy({
                where: { id: id }
            })
            let y = await UserBiodata.destroy({
                where: { id: id }
            })
        } catch (e) {
            console.log(`error: ${e.message}`)
        }
    },
}