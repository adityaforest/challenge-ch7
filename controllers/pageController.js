module.exports = {
    home: (req, res) => {
        res.render('index')
    },
    dashboard: (req, res) => {
        res.render('dashboard')
    },
    login: (req, res) => {
        res.render('login', { message: "" })
    },
    register: (req, res) => {
        res.render('register')
    },
    game: {
        rps: (req, res) => {
            res.render('game_rps')
        }
    }
}