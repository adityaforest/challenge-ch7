const passport = require("../lib/passport")
const userDb = require('../services/user_db')
const crypt = require('../lib/crypt')

module.exports = {
    loginShow: (req, res) => {
        res.render('login', { message: "" })
    },
    loginPost: passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }),
    logout: (req, res, next) => {
        req.logout(err => {
            if (err) return next()
            res.redirect('/login')
        })
    },
    registerShow: (req, res) => {
        res.render('register')
    },
    registerPost: async (req, res) => {
        let payload = req.body   
        payload.password = crypt.encrypt(req.body.password)        
        payload.authority = "PlayerUser"
        await userDb.createUserData(payload)
        res.redirect('/login')
    }
}