'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('UserHistories', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      roomid: {
        type: Sequelize.INTEGER
      },
      player1id: {
        type: Sequelize.INTEGER
      },
      player1username: {
        type: Sequelize.STRING
      },
      player2id: {
        type: Sequelize.INTEGER
      },
      player2username: {
        type: Sequelize.STRING
      },
      role: {
        type: Sequelize.STRING
      },
      round1: {
        type: Sequelize.STRING
      },
      round2: {
        type: Sequelize.STRING
      },
      round3: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('UserHistories');
  }
};