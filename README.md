Cara menjalankan app
1. Instal dependensi yang diperlukan :
```
yarn install 
```
2. edit file /config/config.json sesuaikan dengan environment database di komp anda

3. bikin db
```
yarn create 
```
4. migrasi isi database :
```
yarn migrate 
```
5. Isi data awal (admin , id:1) :
```
yarn seed 
```
6. Start server
```
yarn start
```
7. Server telah running di port 8000 , buka browser anda
```
localhost:8000/
```

LIST ENDPOINT
```
/
/dashboard
/login
/register
/game/rps
```
