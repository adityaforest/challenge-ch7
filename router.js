const router = require('express').Router()

//controllers
const auth = require('./controllers/authController')
const page = require('./controllers/pageController')
const user = require('./controllers/userController')
const history = require('./controllers/historyController')
const room = require('./controllers/roomController')

//pages show
router.get('/', page.home)
router.get('/login', page.login)
router.get('/register', page.register)
router.get('/dashboard', page.dashboard)
router.get('/game/rps',  page.game.rps)

//api auth 
router.post('/api/v1/login', auth.login)
router.post('/api/v1/verifyToken',auth.verifyToken)

//api user
router.post('/api/v1/users',user.create)
router.get('/api/v1/users',user.readAll)
router.get('/api/v1/users/:id',user.readById)
router.put('/api/v1/users/:id',user.update)
router.delete('/api/v1/users/:id',user.delete)

//api history
router.post('/api/v1/history',history.create)
router.get('/api/v1/history',history.readAll)
router.get('/api/v1/history/:userid',history.readByUserId)
router.delete('/api/v1/history/:id',history.delete)

//api room
router.post('/api/v1/rooms/enter', room.enter)
router.get('/api/v1/rooms/match/:id/:username', room.match)
router.delete('/api/v1/rooms/delete/:id', room.delete)

//api game
router.get('/api/v1/rooms/check/:roomid', room.checkGameDetail)
router.post('/api/v1/rooms/setchoice', room.playerChoose)

module.exports = router