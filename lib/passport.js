const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const user = require('../controllers/userController')

const validator = async (payload, done) => {
    console.log(JSON.stringify(payload))
    let userData = await user.readByUsername(payload)
    if (userData) {
        return done(null, userData)
    }

    done(new Error('invalid user'), false)
}

const strategy = new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: 's3cr3t'
}, validator)

passport.use(strategy)

module.exports = passport