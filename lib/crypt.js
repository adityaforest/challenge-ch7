const bcrypt = require('bcrypt')

module.exports = {
    encrypt: (password) => {
        return bcrypt.hashSync(password, 10)        
    },
    compare: (passowrdInput,passwordDb) => {
        return bcrypt.compareSync(passowrdInput, passwordDb)
    }
}