const passport = require('passport')
const { Strategy: LocalStrategy } = require('passport-local')
const userDb = require('../services/user_db')
const crypt = require('../lib/crypt')

const validator = async (username, password, done) => {
    let userData = await userDb.readUserDataByUsername(username)
    console.log(userData)
    if (userData) {
        if (crypt.compare(password,userData.password)) return done(null, userData)
    }

    done(new Error('invalid user'), false)
}

const strategy = new LocalStrategy({
    usernameField: 'username', //name di element html input untuk username
    passwordField: 'password'
}, validator)

passport.use(strategy)

passport.serializeUser((user, done) => done(null, user.id))
passport.deserializeUser(async (id, done) => {
    let userData = await userDb.readUserData(id)
    // console.log(userData)
    if (userData) {
        if (id == userData.id) {
            return done(null, userData)
        }
    }

    done(new Error('unknown user'), false)
})

module.exports = passport