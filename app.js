// DARI CHAP SEBELUMNYA
const express = require('express')
const app1 = express()
const { PORT = 8000 } = process.env
const passport = require('./lib/passport')
const path = require('path')
const router = require('./router.js')

app1.use('/public', express.static(path.join('./public')))
app1.use(express.urlencoded({ extended: false }))
app1.use(express.json())
app1.set('view engine', 'ejs')
app1.use(passport.initialize())
app1.use(router)

app1.listen(PORT, () => console.log(`main server running on port ${PORT}`))
