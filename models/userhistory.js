'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // UserHistory.hasOne(models.User,{ as:'userdata', foreignKey:'id'})
      // UserHistory.belongsTo(models.User,{ as:'userdata',foreignKey:'userid'})
    }
  }
  UserHistory.init({
    roomid:DataTypes.INTEGER,         
    player1id: DataTypes.INTEGER,
    player1username: DataTypes.STRING,
    player2id: DataTypes.INTEGER,
    player2username: DataTypes.STRING,
    role: DataTypes.STRING,
    round1: DataTypes.STRING,
    round2: DataTypes.STRING,
    round3: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserHistory',
  });
  return UserHistory;
};