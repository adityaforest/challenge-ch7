'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasOne(models.UserBiodata,{ as:'biodata',foreignKey:'id'})
      // User.belongsTo(models.UserHistory,{foreignKey:'userid'})
      // User.hasOne(models.UserHistory ,{foreignKey:'userid'})      
    }
  }
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    authority: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};