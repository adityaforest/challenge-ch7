'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('UserBiodata', [{
      id: 1,
      email: "admin@mail.com",
      firstName: "admin",
      lastName: "admin",
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: 2,
      email: "user1@mail.com",
      firstName: "user",
      lastName: "satu",
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: 3,
      email: "user2@mail.com",
      firstName: "user",
      lastName: "dua",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('UserBiodata', null, { id: [1,2,3] });
  }
};
