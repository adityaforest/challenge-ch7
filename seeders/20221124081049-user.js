'use strict';
const crypt = require('../lib/crypt')

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Users', [{
      id: 1,
      username: "admin",
      password: crypt.encrypt("admin"),
      authority : "SuperAdmin",
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: 2,
      username: "user1",
      password: crypt.encrypt("user1"),
      authority : "PlayerUser",
      createdAt: new Date(),
      updatedAt: new Date(),
    },{
      id: 3,
      username: "user2",
      password: crypt.encrypt("user2"),
      authority : "PlayerUser",
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, { id: [1,2,3] });
  }
};
