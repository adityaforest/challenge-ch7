//================================= AUTHENTICATE ======================
const BASE_URL = 'http://localhost:8000'
let userdata = JSON.parse(window.localStorage.getItem('userdata'))
//VALIDATE FIRST !
if (userdata) {
    fetch(`${BASE_URL}/api/v1/verifyToken`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            token: userdata.token
        })
    })
        .then(res => res.json())
        .then(res => {
            console.log(res)
            if (!res.tokenValid) {
                alert('Token invalid ! Please login first !')
                window.location.replace(`${BASE_URL}/login`)
            }
        })
}
else {
    alert('Please login first !')
    window.location.replace(`${BASE_URL}/login`)
}

//============================= GAME =================================
var isPlayerAlreadyChoose = false;
var com, player;
var resultAudio;
var result;
const WARNA_BG_VIS = "#c4c4c4";
const WARNA_BG_INVIS = "#c4c4c400";
const modal = new bootstrap.Modal(document.querySelector(".modal"), {})
const modalBody = document.getElementById('modal-body')
const roomInfo = document.getElementById('room-info')
let roomData = JSON.parse(window.localStorage.getItem('roomData'))
let currentRound = 1
let thisGameResult = ["-", "-", "-"]
//============================= FUNCTIONS ============================================
refreshThisGameWinner = () => {
    document.getElementById('winner-info').innerHTML = `
    <p>Round 1 : ${thisGameResult[0]}</p>
    <p>Round 2 : ${thisGameResult[1]}</p>
    <p>Round 3 : ${thisGameResult[2]}</p>
    `
}

refreshPage = () => {
    currentRound++
    document.getElementById("versus-showcase").style.display = "flex";
    document.getElementById("result-showcase").style.display = "none";
    document.getElementById("next-round").style.display = "none";
    document.getElementById("batu-com").style.backgroundColor = WARNA_BG_INVIS
    document.getElementById("kertas-com").style.backgroundColor = WARNA_BG_INVIS
    document.getElementById("gunting-com").style.backgroundColor = WARNA_BG_INVIS
    document.getElementById("batu-player").style.backgroundColor = WARNA_BG_INVIS
    document.getElementById("kertas-player").style.backgroundColor = WARNA_BG_INVIS
    document.getElementById("gunting-player").style.backgroundColor = WARNA_BG_INVIS
    isPlayerAlreadyChoose = false
    document.getElementById("round").innerHTML = `ROUND ${currentRound}`
    if (currentRound <= 2) {
        document.getElementById("next-round-text").innerHTML = `NEXT ROUND`
    }
    else {
        document.getElementById("next-round-text").innerHTML = `FINISH (BACK TO HOME)`
    }
}

nextRound = () => {
    if (currentRound <= 2) {
        refreshPage()
    }
    else {
        preparedRoomData = {...roomData}
        delete preparedRoomData.gameDetail
        delete preparedRoomData.matched
        preparedRoomData.round1 = thisGameResult[0]
        preparedRoomData.round2 = thisGameResult[1]
        preparedRoomData.round3 = thisGameResult[2]
        preparedRoomData.role = myRole()

        fetch(`${BASE_URL}/api/v1/history`, {
            method: "POST",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify(preparedRoomData)
        })
            .then(window.localStorage.removeItem('roomData'))
            .then(window.location.replace(`${BASE_URL}/`))
    }
}

let source
function enterRoom() {
    fetch(`${BASE_URL}/api/v1/rooms/enter`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            id: userdata.id,
            username: userdata.username
        })
    })
        .then(res => res.json())
        .then(res => {
            console.log(res.id)
            source = new EventSource(`${BASE_URL}/api/v1/rooms/match/${res.id}/${userdata.username}`)
            source.addEventListener("roomMatching", (e) => {
                // console.log(e.data)
                refreshRoomData(e.data)
                if (JSON.parse(e.data).matched == true) {
                    setRoomInfo(e.data)
                    modal.hide()
                    modalBody.innerHTML = `
                    <h5>MUSUH LAGI BINGUNG MILIH , SABAR YACH ! </h5>                   
                    `
                    source.close()
                }
            })
        })
}

setRoomInfo = (roomData) => {
    let x = JSON.parse(roomData)
    roomInfo.innerHTML = `
    <p> Room ID : ${x.id}</p>
    <p> Player 1 : ${x.player1username} (id:${x.player1id})</p>
    <p> Player 2 : ${x.player2username} (id:${x.player2id})</p>
    `
}

refreshRoomData = (roomData) => {
    window.localStorage.setItem('roomData', roomData)
    let x = JSON.parse(roomData)
    modalBody.innerHTML = `
                    <h5>ROOM ID : ${x.id} </h5>
                    <h5>MATCHED : ${x.matched} </h5>
                    <h5>PLAYER 1 ID : ${x.player1id} </h5>
                    <h5>PLAYER 1 USERNAME : ${x.player1username} </h5>
                    <h5>PLAYER 2 ID : ${x.player2id} </h5>
                    <h5>PLAYER 2 USERNAME : ${x.player2username} </h5>
                    `
}
modal.show()
enterRoom()

myRole = () => {
    if (userdata.id == roomData.player1id) {
        return "P1"
    }
    else if (userdata.id == roomData.player2id) {
        return "P2"
    }
}

function checkGameDetail(repeatTime) {
    modal.show()
    setTimeout(() => {
        fetch(`${BASE_URL}/api/v1/rooms/check/${roomData.id}`)
            .then(res => res.json())
            .then(res => {
                console.log(res)
                if (res[currentRound - 1].winner) {
                    modal.hide()
                    if (myRole() == 'P1') {
                        player = res[currentRound - 1].p1choice
                        console.log("PLAYER : " + player)

                        com = res[currentRound - 1].p2choice
                        console.log("COM : " + com);
                    }
                    else if (myRole() == 'P2') {
                        player = res[currentRound - 1].p2choice
                        console.log("PLAYER : " + player)

                        com = res[currentRound - 1].p1choice
                        console.log("COM : " + com);
                    }

                    let winner = res[currentRound - 1].winner
                    if (winner == myRole()) {
                        result = "WIN"
                        resultAudio = document.getElementById("audio-result-win");
                    }
                    else if (winner == "DRAW") {
                        result = "DRAW"
                        resultAudio = document.getElementById("audio-result-draw");
                    }
                    else {
                        result = "LOSE"
                        resultAudio = document.getElementById("audio-result-lose");
                    }
                    document.getElementById("result-text").innerHTML = result;
                    console.log("RESULT : " + result);
                    startComChoosingAnimation();
                }
                else {
                    checkGameDetail(repeatTime)
                }
            })
    }, repeatTime)
}

function playerChoose(clickedChoice) {
    if (isPlayerAlreadyChoose) return;
    isPlayerAlreadyChoose = true;

    document.getElementById("audio-choose").play();

    fetch(`${BASE_URL}/api/v1/rooms/setchoice`, {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            roomid: roomData.id,
            round: currentRound - 1,
            role: myRole(),
            choice: clickedChoice
        })
    })

    checkGameDetail(500)
}

function clickBatu() {
    playerChoose("R");
}

function clickKertas() {
    playerChoose("P");
}

function clickGunting() {
    playerChoose("S");
}

//====================== ANIMATION BIAR KAYAK WEB JUDI ONLINE ==================================
var i = 0,
    bgArray = [
        document.getElementById("batu-com"),
        document.getElementById("kertas-com"),
        document.getElementById("gunting-com")
    ];

function startComChoosingAnimation() {
    document.getElementById("roulette-audio-opening").play();
    animationStart(100, 69, fastToSlow);
}

function animationStart(fractionTime, multiplier, afterState) {
    var interval = setInterval(transitionActivity, fractionTime);
    setTimeout(function () {
        clearInterval(interval);
        afterState();
    }, fractionTime * multiplier);
}

function transitionActivity() {
    let x = i - 1;
    if (x < 0) x = bgArray.length - 1;
    if (i > bgArray.length - 1) i = 0;

    bgArray[i].style.backgroundColor = WARNA_BG_VIS;
    bgArray[x].style.backgroundColor = WARNA_BG_INVIS;

    i++;
}

function fastToSlow() {
    if (com == "R") {
        animationStart(500, 4, showResult);
        document.getElementById("roulette-audio-endingLong").play();
    }
    else if (com == "P") {
        animationStart(500, 2, showResult);
        document.getElementById("roulette-audio-endingShort").play();
    }
    else if (com == "S") {
        animationStart(500, 3, showResult);
        document.getElementById("roulette-audio-endingMed").play();
    }
}

function showResult() {
    if (com == "R") {
        bgArray[0].style.backgroundColor = WARNA_BG_VIS;
        bgArray[1].style.backgroundColor = WARNA_BG_INVIS;
        bgArray[2].style.backgroundColor = WARNA_BG_INVIS;
    }
    else if (com == "P") {
        bgArray[1].style.backgroundColor = WARNA_BG_VIS;
        bgArray[0].style.backgroundColor = WARNA_BG_INVIS;
        bgArray[2].style.backgroundColor = WARNA_BG_INVIS;
    }
    else if (com == "S") {
        bgArray[2].style.backgroundColor = WARNA_BG_VIS;
        bgArray[1].style.backgroundColor = WARNA_BG_INVIS;
        bgArray[0].style.backgroundColor = WARNA_BG_INVIS;
    }
    resultAudio.play();
    document.getElementById("versus-showcase").style.display = "none";
    document.getElementById("result-showcase").style.display = "flex";
    document.getElementById("next-round").style.display = "flex";
    gsap.fromTo('#result-showcase', { opacity: 0, scale: 3, rotation: '180deg' }, { opacity: 1, scale: 1, rotation: '-30deg' });
    thisGameResult[currentRound - 1] = result
    refreshThisGameWinner()
}

//============================== EVENTLISTENER FOR BUTTON MOUSEOVER AND MOUSEOUT =============================================

document.getElementById("batu-player").addEventListener("mouseover", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("batu-player").style.backgroundColor = WARNA_BG_VIS;
});

document.getElementById("batu-player").addEventListener("mouseout", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("batu-player").style.backgroundColor = WARNA_BG_INVIS;
});

document.getElementById("kertas-player").addEventListener("mouseover", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("kertas-player").style.backgroundColor = WARNA_BG_VIS;
});

document.getElementById("kertas-player").addEventListener("mouseout", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("kertas-player").style.backgroundColor = WARNA_BG_INVIS;
});

document.getElementById("gunting-player").addEventListener("mouseover", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("gunting-player").style.backgroundColor = WARNA_BG_VIS;
});

document.getElementById("gunting-player").addEventListener("mouseout", function () {
    if (!isPlayerAlreadyChoose) document.getElementById("gunting-player").style.backgroundColor = WARNA_BG_INVIS;
});

